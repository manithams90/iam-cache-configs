# Keycloak with Infinispan on Kubernetes (Kind)

This repository provides a guide and configuration files to deploy the Keycloak and Infinispan on a Kubernetes cluster running in KIND (Kubernetes IN Docker) using Terraform for infrastructure provisioning and Helm for application deployment.

## Features
- `kind/`: Configuration file for KinD tool to create a  local Kubernetes cluster using Docker.
- `terraform/`: Directory containing terraform scripts to provision kubernetes componenets (ingress-nginx, keycloak, Infinispan).

## Prerequisites
Ensure you have the following prerequisites installed:
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) - (v0.22.0 go1.21.7)
- [Terraform](https://developer.hashicorp.com/terraform/install?product_intent=terraform) (version v1.7.4 or later)
- [kubectl](https://kubernetes.io/docs/tasks/tools/) (latest)

## Provision Infrastructure with KinD & Terraform
1. **Kind**: Navigate to the `kind/` directory and update the `doodle-config.yaml` accordingly and then execute the following commands:
``````
kind create cluster --config kind/lingoda-config.yaml
``````
2. **Terraform**: Upon creating the cluster navigate to the `terraform/` directory and execute the following commands:
- Note: Makesure to update the terraform state file location. Currently this is configuered to store the terraform state in a remote S3 bucket.
in ` /provider.tf`
````
 ...
  backend "s3" {
    bucket = "doodle-tf-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
 ...
```` 
Use following commands to run terraform scripts
```
terraform init
terraform plan
terraform apply   
```

## Access Keycloak and Infinispan UI
Update your local `/etc/hosts` file to map the service URL to the Ingress IP address. Open the `/etc/hosts` file in a text editor and add the following line:
```
127.0.0.1       localhost keycloak-admin.example.com keycloak.example.com infinispan.example.com
```
Access Keycloak admin concole:  using keycloak-admin.example.com in your browser.
Access Infinispan concole: using infinispan.example.com in your browser.


## Implementation

I have used the Bitnami Helm charts for KeyCloak and OpenShift Helm Charts for  Infinispan.
Reason behind this selection is they are carefully engineered, actively maintained and are the quickest and easiest way to deploy containers on a Kubernetes cluster that are ready to handle production workloads.

## Configuration

### Keycloak
For this setup I have used the postgres DB as the database of of the Keycloak server. Therefore in here I enabled  the PostgreSQL helm chart. Credentials for the DB server hardcoded in the values.yaml file which is not acceptable in production setup. We can create a kubernetes secret with relevent sensative information and use it with `postgresql.auth.existingSecret` value.

Since  ingress-nginx controller has used to expose the services, I have set proxy to edge in the keycloak configs. Therefore SSL offload can be moved to ingress. 

Keycloak provides a cache configuration file with sensible defaults located at conf/cache-ispn.xml. Cache configuration XML can be found here: ./terraform/kubernetes_components/keycloak/cache-ispn.xml. Following distributed caching configurations can be found in this file:
    - sessions
    - authenticationSessions
    - offlineSessions
    - clientSessions
    - offlineClientSessions
    - loginFailures
    - actionTokens

The following block defines the remote infinspan server configurations for the distributed caching. (authentication for the remote Infinispan caches)

```
        ...
        <remote-server host="infinispan.infinispan.svc.cluster.local" port="11222" />
                <security>
                    <authentication server-name="infinispan">
                        <digest username="developer" password="BA3hvNx7" realm="default"/>
                    </authentication>
                </security>
        ...
```

### Infinispan
For this setup the infinispan server deployed with the default configurations from the Helm Chart. Used ingress to expose the Infinispan admn concole via `infinispan.example.com`
configuration for the infinispan server can be found here: `./terraform/kubernetes_components/infinispan/infinispan.xml`. This includes the cache configurations.
Authentication configurations are derived from the `users.properties`

## Implementation Status

🚧 **Work in Progress** 🚧

Please note that this project is currently under active development, cache config in remote infinispan server feature may not be functional. 