##################
##  Namespaces ##
#################
resource "kubernetes_namespace" "this" {

  for_each = local.namespaces

  metadata {
    labels = merge(
      local.common_tags,
      try(each.value.labels, {})
    )
    name = each.key
  }
}

locals {
  common_tags = merge(
    {
      environment = var.environment
    },
    var.default_tags
  )
  namespaces = {
    "ingress-nginx" = {}
    "keycloak"      = {}
    "infinispan"    = {}
  }
}

# #################
# ## Helm Charts ##
# #################
# ingress-nginx
resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.10.0"

  namespace        = kubernetes_namespace.this["ingress-nginx"].id
  create_namespace = false

  values = [
    file("${path.module}/kubernetes_components/ingress-nginx/values.yaml")
  ]

  depends_on = [
    kubernetes_namespace.this
  ]
}

# Keycloak
resource "helm_release" "keycloak" {
  name             = "keycloak"
  repository       = "https://charts.bitnami.com/bitnami"
  version          = "19.2.0"
  chart            = "keycloak"
  namespace        = kubernetes_namespace.this["keycloak"].id
  create_namespace = false
  max_history      = 10

  values = [
    file("${path.module}/kubernetes_components/keycloak/values.yaml")
  ]

  depends_on = [
    kubernetes_namespace.this,
    helm_release.ingress_nginx
  ]
}

# Infinispan
resource "helm_release" "infinispan" {
  name             = "infinispan"
  repository       = "https://charts.openshift.io/"
  version          = "0.3.2"
  chart            = "infinispan-infinispan"
  namespace        = kubernetes_namespace.this["infinispan"].id
  create_namespace = false
  max_history      = 10

  values = [
    file("${path.module}/kubernetes_components/infinispan/values.yaml")
  ]

  depends_on = [
    kubernetes_namespace.this,
    helm_release.keycloak
  ]
}